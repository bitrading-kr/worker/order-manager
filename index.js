const VERSION = '1.1.8';
const ORDER_VERSION = '1.0.0';
const semver = require('semver');

const { validator } = require('bitrading-module-manager');
const moment = require('moment'),
      _ = require('lodash');

const Order = require('./lib/Order.js');

class OrderManager {
  constructor (orderList) {
    this.orders = orderList;
  }

  get version () {
    return VERSION;
  }

  async each (func) {
    for (var i in this.orders) {
      let order = this.orders[i];
      let result = await func.call(order);
      if (result != null) {
        this.orders[i] = new Order(result);
      }
    }
  }

  eachSync (func) {
    for (var i in this.orders) {
      let order = this.orders[i];
      let result = func.call(order);
      if (result != null) {
        this.orders[i] = new Order(result);
      }
    }
  }

  extractCoins (list) {
    if (!Array.isArray(list)) list = this.orders;
    return list.map(elem => elem.coin);
  }

  extractMarkets (list) {
    if (!Array.isArray(list)) list = this.orders;
    return list.map(elem => elem.market);
  }

  filterByExchange (exchange, list) {
    if (!Array.isArray(list)) list = this.orders;
    return list.filter(elem => elem.exchange == exchange);
  }

  filterByMarket (market, list) {
    if (!Array.isArray(list)) list = this.orders;
    return list.filter(elem => elem.market == market);
  }

  filterByCoin (coin, list) {
    if (!Array.isArray(list)) list = this.orders;
    return list.filter(elem => elem.coin == coin);
  }

  filterBy (key, value, list) {
    if (!Array.isArray(list)) list = this.orders;
    return list.filter(elem => _.get(elem, key) == value);
  }

  set orders (orders) {
    this._orders = orders.map(elem => {
      if (!semver.satisfies(ORDER_VERSION, elem.version)) {
        throw new Error(`버전이 맞지 않습니다. 현재 버전 : ${ORDER_VERSION}, Order 데이터 버전 : ${elem.version}`);
      }
      return new Order(elem);
    });
  }
  get orders () {
    return this._orders;
  }

}

module.exports = OrderManager;
