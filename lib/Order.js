const { validator, askingPrice } = require('bitrading-module-manager');
const moment = require('moment'),
      _ = require('lodash'),
      Trading = require('./Trading.js');

class Order {
  constructor (order) {
    this.order = order;
  }

  async eachEntries (func) {
    let entry = this.order.entry,
        strategy = entry.strategy;
    for (var i in strategy) {
      let data = strategy[i];
      let result = await func.call(this, {
        tradings: data.tradings,
        price: data.price,
        ratio: data.ratio,
      });
      if (result != null) {
        strategy[i] = validator('order/entry/strategy', result);
      }
    }
    this.entry = entry;
  }

  eachEntriesSync (func) {
    let entry = this.order.entry,
        strategy = entry.strategy;
    for (var i in strategy) {
      let data = strategy[i];
      let result = func.call(this, {
        tradings: data.tradings,
        price: data.price,
        ratio: data.ratio,
      });
      if (result != null) {
        strategy[i] = validator('order/entry/strategy', result);
      }
    }
    this.entry = entry;
  }

  async eachTargets (func) {
    let target = this.order.target,
        strategy = target.strategy;
    for (var i in strategy) {
      let data = strategy[i];
      let result = await func.call(this, {
        tradings: data.tradings,
        price: data.price,
        ratio: data.ratio,
      });
      if (result != null) {
        strategy[i] = validator('order/target/strategy', result);
      }
    }
    this.target = target;
  }

  eachTargetsSync (func) {
    let target = this.order.target,
        strategy = target.strategy;
    for (var i in strategy) {
      let data = strategy[i];
      let result = func.call(this, {
        tradings: data.tradings,
        price: data.price,
        ratio: data.ratio,
      });
      if (result != null) {
        strategy[i] = validator('order/target/strategy', result);
      }
    }
    this.target = target;
  }

  set version (version) {
    this._order.version = validator('order/version', version);
  }
  get version () {
    return this._order.version;
  }

  set id (id) {
    this._order.id = validator('order/id', id);
  }
  get id () {
    return this._order.id;
  }

  set user_id (user_id) {
    this._order.user_id = validator('order/user_id', user_id);
  }
  get user_id () {
    return this._order.user_id;
  }

  set title (title) {
    this._order.title = validator('order/title', title);
  }
  get title () {
    return this._order.title;
  }

  set desc (desc) {
    this._order.desc = validator('order/desc', desc);
  }
  get desc () {
    return this._order.desc;
  }

  set author (author) {
    this._order.author = validator('order/author', author);
  }
  get author () {
    return this._order.author;
  }

  set source_id (source_id) {
    this._order.source_id = validator('order/source_id', source_id);
  }
  get source_id () {
    return this._order.source_id;
  }

  set exchange (exchange) {
    this._order.exchange = validator('order/exchange', exchange);
  }
  get exchange () {
    return this._order.exchange;
  }

  set market (market) {
    this._order.market = validator('order/market', market);
  }
  get market () {
    return this._order.market;
  }

  set coin (coin) {
    this._order.coin = validator('order/coin', coin);
  }
  get coin () {
    return this._order.coin;
  }

  set created_at (created_at) {
    this._order.created_at = validator('order/datetime', created_at);
  }
  get created_at () {
    return this._order.created_at;
  }

  set updated_at (updated_at) {
    try {
      this._order.updated_at = validator('order/datetime', updated_at);
    } catch (e) {
      this._order.updated_at = null;
    }
  }
  get updated_at () {
    return this._order.updated_at;
  }

  set finished_at (finished_at) {
    try {
      this._order.updated_at = validator('order/datetime', finished_at);
    } catch (e) {
      this._order.updated_at = null;
    }
  }
  get finished_at () {
    return this._order.finished_at;
  }

  set state (state) {
    this._order.state = validator('order/state', state);
  }
  get state () {
    return this._order.state;
  }

  set status (status) {
    this._order.status = validator('order/status', status);
  }
  get status () {
    return this._order.status;
  }

  set running_state (running_state) {
    this._order.running_state = validator('order/running_state', running_state);
  }
  get running_state () {
    return this._order.running_state;
  }

  set entry (entry) {
    let self = this;
    let e = validator('order/entry', entry);
    entry.strategy = this._convertStrategy(e.strategy);
    this._validateSumRatio(entry.strategy);
    entry.strategy = entry.strategy.map(elem => {
      elem.tradings = self._convertTradings(elem.tradings);
      return elem;
    });
    this._order.entry = entry;
  }
  get entry () {
    return this._order.entry;
  }

  set target (target) {
    let self = this;
    let t = validator('order/target', target);
    target.strategy = this._convertStrategy(t.strategy);
    this._validateSumRatio(target.strategy);
    target.strategy = target.strategy.map(elem => {
      elem.tradings = self._convertTradings(elem.tradings);
      return elem;
    });
    this._order.target = target;
  }
  get target () {
    return this._order.target;
  }

  set stoploss (stoploss) {
    let self = this;
    let tmpSL = validator('order/stoploss', stoploss);
    switch (tmpSL.type) {
    case 1:
      tmpSL.strategy.stop = askingPrice.trim(self.exchange, self.market, tmpSL.strategy.stop);
      tmpSL.strategy.limit = askingPrice.trim(self.exchange, self.market, tmpSL.strategy.limit);
      break;
    }

    if (_.isPlainObject(tmpSL.strategy)) {
      tmpSL.strategy.tradings = self._convertTradings(tmpSL.strategy.tradings);
    }
    this._order.stoploss = tmpSL;
  }
  get stoploss () {
    return this._order.stoploss;
  }

  _convertStrategy (strategy) {
    let self = this;
    let trimedStrategy = _.map(strategy, elem => {
      elem.price = askingPrice.trim(self.exchange, self.market, elem.price);
      return elem;
    });
    return _.orderBy(_.uniqBy(_.orderBy(strategy, o => o.ratio, ['desc']), o => o.price), o => o.price);
  }

  _validateSumRatio (strategy) {
    let sum = _.sumBy(strategy, o => o.ratio);
    sum = parseFloat(parseFloat(sum).toFixed(5));
    return validator('ratio', sum);
  }

  _convertTradings (tradings) {
    if (_.isArray(tradings)) {
      return tradings.map(elem => {
        if (typeof elem === 'object') {
          return new Trading(elem);
        }
        return elem;
      });
    } else {
      return tradings;
    }
  }

  set order (order) {
    if (!_.isPlainObject(this._order)) this._order = {};
    this.version = order.version;
    this.id = order.id;
    this.user_id = order.user_id;
    this.title = order.title;
    this.desc = order.desc;
    this.author = order.author;
    this.source_id = order.source_id;
    this.exchange = order.exchange;
    this.market = order.market;
    this.coin = order.coin;
    this.created_at = order.created_at;
    this.updated_at = order.updated_at;
    this.finished_at = order.finished_at;
    this.state = order.state;
    this.status = order.status;
    this.running_state = order.running_state;
    this.entry = order.entry;
    this.target = order.target;
    this.stoploss = order.stoploss;
  }
  get order () {
    return this._order;
  }
}

module.exports = Order;
