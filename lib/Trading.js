const { validator } = require('bitrading-module-manager');
const _ = require('lodash');
const semver = require('semver');
const moment = require('moment');

const TRADING_VERSION = '1.0.0'; // 함부로 바꾸지 말 것

class Trading {
  constructor (trading) {
    this.trading = trading;
  }

  set version (version) {
    if (!semver.satisfies(TRADING_VERSION, version)) {
      throw new Error(`버전이 맞지 않습니다. 현재 버전 : ${TRADING_VERSION}, Trading 데이터 버전 : ${version}`);
    }
    this._trading.version = validator('trading/version', version);
  }
  get version () {
    return this._trading.version;
  }

  set id (id) {
    this._trading.id = validator('trading/id', id);
  }
  get id () {
    return this._trading.id;
  }

  set order_id (order_id) {
    this._trading.order_id = validator('trading/id', order_id);
  }
  get order_id () {
    return this._trading.order_id;
  }

  set user_id (user_id) {
    this._trading.user_id = validator('trading/user_id', user_id);
  }
  get user_id () {
    return this._trading.user_id;
  }

  set exchange (exchange) {
    this._trading.exchange = validator('trading/exchange', exchange);
  }
  get exchange () {
    return this._trading.exchange;
  }

  set market (market) {
    this._trading.market = validator('trading/market', market);
  }
  get market () {
    return this._trading.market;
  }

  set coin (coin) {
    this._trading.coin = validator('trading/coin', coin);
  }
  get coin () {
    return this._trading.coin;
  }

  set state (state) {
    this._trading.state = validator('trading/state', state);
  }
  get state () {
    return this._trading.state;
  }

  set side (side) {
    this._trading.side = validator('trading/side', side);
  }
  get side () {
    return this._trading.side;
  }

  set created_at (created_at) {
    this._trading.created_at = validator('trading/datetime', created_at);
  }
  get created_at () {
    return this._trading.created_at;
  }

  set checked_at (checked_at) {
    this._trading.checked_at = validator('trading/datetime', checked_at);
  }
  get checked_at () {
    return this._trading.checked_at;
  }

  set updated_at (updated_at) {
    this._trading.updated_at = validator('trading/datetime', updated_at);
  }
  get updated_at () {
    return this._trading.updated_at;
  }

  set fee (fee) {
    this._trading.fee = validator('trading/fee', fee);
  }
  get fee () {
    return this._trading.fee;
  }

  set volume (volume) {
    this._trading.volume = validator('trading/volume', volume);
  }
  get volume () {
    return this._trading.volume;
  }

  set price (price) {
    this._trading.price = validator('trading/price', price);
  }
  get price () {
    return this._trading.price;
  }

  set info (info) {
    this._trading.info = info;
  }
  get info () {
    return this._trading.info;
  }

  set trading (trading) {
    if (!_.isPlainObject(this._trading)) this._trading = {};

    this.version = trading.version;
    this.id = trading.id;
    this.order_id = trading.order_id;
    this.user_id = trading.user_id;
    this.state = trading.state;
    this.side = trading.side;
    this.created_at = trading.created_at;
    this.checked_at = trading.checked_at;
    this.updated_at = trading.updated_at;
    this.exchange = trading.exchange;
    this.market = trading.market;
    this.coin = trading.coin;
    this.info = trading.info;
    this.fee = trading.fee;
    this.volume = trading.volume;
    this.price = trading.price;
  }
  get trading () {
    return this._trading;
  }
}

module.exports = Trading;
