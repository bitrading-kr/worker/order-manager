const assert = require('assert');
const _ = require('lodash');

const moment = require('moment');
const OrderManager = require('./index.js');

function genOrder (exchange, market, coin, quantity, entryStrategy, targetStrategy, stopLimit) {
  return {
    version: '1',
    id: '1',
    userId: 'test',
    exchange: exchange,
    market: market,
    coin: coin,
    entry: {
      quantity: quantity,
      strategy: entryStrategy
    },
    target: {
      strategy: targetStrategy
    },
    stoploss: {
      type: 1,
      strategy: stopLimit
    }
  };
}

let ADA = genOrder('UPBIT', 'KRW', 'ADA', 50000,
                   [
                     {
                       tradings: [],
                       price: 81.0,
                       ratio: 0.3
                     },
                     {
                       tradings: [],
                       price: 81.0,
                       ratio: 0.4
                     },
                     {
                       tradings: [],
                       price: 82.0,
                       ratio: 0.2
                     },
                     {
                       tradings: [],
                       price: 83.0,
                       ratio: 0.4
                     },
                     {
                       tradings: [],
                       price: 83.0,
                       ratio: 0.3
                     }
                   ],
                   [
                     {
                       tradings: [],
                       price: 83.0,
                       ratio: 0.3
                     },
                     {
                       tradings: [],
                       price: 84.0,
                       ratio: 0.4
                     },
                     {
                       tradings: [],
                       price: 85.0,
                       ratio: 0.3
                     }
                   ],
                   {
                     tradings: [],
                     ratio: 0.7,
                     stop: 80.0,
                     limit: 78.0
                   });

let ADA2 = genOrder('UPBIT', 'KRW', 'ADA', 50000,
                   [
                     {
                       tradings: [],
                       price: 81.0,
                       ratio: 0.3
                     },
                     {
                       tradings: [],
                       price: 81.0,
                       ratio: 0.4
                     },
                     {
                       tradings: [],
                       price: 82.0,
                       ratio: 0.2
                     },
                     {
                       tradings: [],
                       price: 83.0,
                       ratio: 0.4
                     },
                     {
                       tradings: [],
                       price: 83.0,
                       ratio: 0.3
                     }
                   ],
                   [
                     {
                       tradings: [],
                       price: 83.0,
                       ratio: 0.3
                     },
                     {
                       tradings: [],
                       price: 84.0,
                       ratio: 0.4
                     },
                     {
                       tradings: [],
                       price: 85.0,
                       ratio: 0.3
                     }
                   ],
                   {
                     tradings: [],
                     ratio: 0.7,
                     stop: 80.0,
                     limit: 78.0
                   });

let ADA3 = genOrder('UPBIT', 'BTC', 'ADA', 50000,
                   [
                     {
                       tradings: [],
                       price: 81.0,
                       ratio: 0.3
                     },
                     {
                       tradings: [],
                       price: 81.0,
                       ratio: 0.4
                     },
                     {
                       tradings: [],
                       price: 82.0,
                       ratio: 0.2
                     },
                     {
                       tradings: [],
                       price: 83.0,
                       ratio: 0.4
                     },
                     {
                       tradings: [],
                       price: 83.0,
                       ratio: 0.3
                     }
                   ],
                   [
                     {
                       tradings: [],
                       price: 83.0,
                       ratio: 0.3
                     },
                     {
                       tradings: [],
                       price: 84.0,
                       ratio: 0.4
                     },
                     {
                       tradings: [],
                       price: 85.0,
                       ratio: 0.3
                     }
                   ],
                   {
                     tradings: [],
                     ratio: 0.7,
                     stop: 80.0,
                     limit: 78.0
                   });

let ADA4 = genOrder('UPBIT', 'BTC', 'ADA', 50000,
                   [
                     {
                       tradings: [],
                       price: 81.12,
                       ratio: 0.3
                     },
                     {
                       tradings: [],
                       price: 81.23,
                       ratio: 0.4
                     },
                     {
                       tradings: [],
                       price: 83.0,
                       ratio: 0.2
                     },
                     {
                       tradings: [],
                       price: 83.0,
                       ratio: 0.3
                     }
                   ],
                   [
                     {
                       tradings: [],
                       price: 83.0,
                       ratio: 0.3
                     },
                     {
                       tradings: [],
                       price: 84.0,
                       ratio: 0.4
                     },
                     {
                       tradings: [],
                       price: 85.0,
                       ratio: 0.3
                     }
                   ],
                   {
                     tradings: [],
                     ratio: 0.7,
                     stop: 80.0,
                     limit: 78.0
                   });

const om = new OrderManager([ADA, ADA2, ADA3, ADA4]);
describe(`#.OrderManager()`, function () {
  describe(`#.each()`, function () {
	  it(`#eachEntries()`, async function () {
      await om.each(async function () {
        await this.eachEntries(async function (data) {
          data.tradings = [{
            created_at: moment('2018-11-03T21:47:04+09:00').format('YYYY-MM-DD HH:mm:ss.SSS Z'),
            checked_at: moment('2018-11-03T21:47:04+09:00').format('YYYY-MM-DD HH:mm:ss.SSS Z'),
            updated_at: moment('2018-11-03T21:47:04+09:00').format('YYYY-MM-DD HH:mm:ss.SSS Z'),
            "coin": "XLM",
            "exchange": "UPBIT",
            "fee": {
              "executed": 0,
              "total": 0.44300000001,
            },
            "id": "-1",
            "orderId": '1',
            "info": {
              "coin": "XLM",
              "created": "2018-11-03T21:47:04+09:00",
              "executedVolume": 0,
              "locked": 886.44300002001,
              "market": "KRW",
              "paidFee": 0,
              "price": 267,
              "remainingFee": 0.44300000001,
              "remainingVolume": 3.31835206,
              "reservedFee": 0.44300000001,
              "side": "bid",
              "state": "wait",
              "tradesCount": 0,
              "type": "limit",
              "uuid": "816afcd0-bc94-4e49-b69f-5278f603f61e",
              "volume": 3.31835206,
            },
            "market": "KRW",
            "price": {
              "asking": 267,
              "executed": 0,
              "locked": 886.44300002001,
            },
            "side": "bid",
            "state": "wait",
            "userId": "test",
            "version": "1",
            "volume": {
              "executed": 0,
              "total": 3.31835206
            }
          }];
          return data;
        });
        await this.eachEntries(async function (data) {
          console.log(data.tradings[0]);
          assert.deepEqual(data.tradings, [{
            "_trading": {
              created_at: moment('2018-11-03T21:47:04+09:00').format('YYYY-MM-DD HH:mm:ss.SSS Z'),
              checked_at: moment('2018-11-03T21:47:04+09:00').format('YYYY-MM-DD HH:mm:ss.SSS Z'),
              updated_at: moment('2018-11-03T21:47:04+09:00').format('YYYY-MM-DD HH:mm:ss.SSS Z'),
              "coin": "XLM",
              "exchange": "UPBIT",
              "fee": {
                "executed": 0,
                "total": 0.44300000001,
              },
              "id": "-1",
              "orderId": "1",
              "info": {
                "coin": "XLM",
                "created": "2018-11-03T21:47:04+09:00",
                "executedVolume": 0,
                "locked": 886.44300002001,
                "market": "KRW",
                "paidFee": 0,
                "price": 267,
                "remainingFee": 0.44300000001,
                "remainingVolume": 3.31835206,
                "reservedFee": 0.44300000001,
                "side": "bid",
                "state": "wait",
                "tradesCount": 0,
                "type": "limit",
                "uuid": "816afcd0-bc94-4e49-b69f-5278f603f61e",
                "volume": 3.31835206,
              },
              "market": "KRW",
              "price": {
                "asking": 267,
                "executed": 0,
                "locked": 886.44300002001,
              },
              "side": "bid",
              "state": "wait",
              "userId": "test",
              "version": "1",
              "volume": {
                "executed": 0,
                "total": 3.31835206
              }
            }
          }]);
        });
      });
    });
	  it(`#eachTargets()`, async function () {
      await om.each(async function () {
        await this.eachTargets(async function (data) {
          data.tradings = [];
          return data;
        });
        await this.eachTargets(async function (data) {
          assert.deepEqual(data.tradings, []);
          console.log(data);
        });
      });
    });
  });
  describe(`#.filter()`, function () {
    it(`filterByExchange`, function () {
      let res = om.filterByExchange('UPBIT');
      console.log(res);
    });
  });
  describe(`#.extractCoins()`, function () {
    it(`()`, function () {
      let res = om.extractCoins();
      console.log(res);
    });
  });
  describe(`#.extractMarkets()`, function () {
    it(`()`, function () {
      let res = om.extractMarkets();
      console.log(res);
    });
  });
});
